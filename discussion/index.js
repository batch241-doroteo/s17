// console.log('Hello!')

// /*
// 	FUNCTIONS
// 	- in javascript are lines/block of clode that tell our device/application to perform a certain task when called/invoke
// 	- are mostly created to create complicated task to run several lines of code in succession
// 	- are also used to prevent repeating lines/block of codes that perform the same task/function.
// */

// // Function declaration
// 	// (Function statement) defines a function with the specified parameters'

/*
	Syntax:
		function functionName() {
			codeblock (statement)
		}

	function keyword - used to defined a javascript functions
	functionName - are made to be able to use later in the code.
	Function block ({}) - statement which compirse the body of the function. This is where the code to be executed.
*/

function printName () {
	console.log('My name is John!');
}
// function Invocation
// codeblock and statement inside a function is not immediately executed when a function is defined. The code block and statements inside a function is executed when the function is invoked or called.
// Invoke/Call the function that we declared.
printName ();

// declaredfunction(); //Uncaught ReferenceError: declaredfunction is not defined

// Function declaration vs Function expression

	// function declation can be created through function declaration by using the function keyword and adding the function name. Declared function are NOT executed immediately. They are "saved for later used" and will be executed later when they are invoked(called)


	// Hoisting
declareFunction();

function declareFunction() {
	console.log("Hello World from the declareFunction");
}
declareFunction();
// Function Expression
	// can also be stored in a variable. This is called function expression.
	// an anonymouse function assigned to the variableFunction
	// Anonymouse function - a function without a name.

	// variableFunction(); //index.js:49 Uncaught ReferenceError: Cannot access 'variableFunction' before initialization

let variableFunction = function() {
	console.log('Hello from the variableFunction');
}
variableFunction();

	// Function expression are ALWAYS invoked(called) using the varibale name.

let funcExpression = function funcName() {
	console.log('Hello from the other side!')
}
funcExpression();

	// Can we reassign declared function and function expression to a NEW anonymous functions? YES!!

declareFunction = function() {
	console.log('updated declareFunction');
}
declareFunction();



const constantFunc = function() {
	console.log('can we reassigned')
}

constantFunc();

	// Function Scoping
	/*
		Scope is the accessibility (visibility) of variables.

		JS Variables has 3 types of scope
			1. Local/block scope
			2. global scope
			3. function scope
	*/
/*
	{
		let localVar = "I am a local variable".;
	}*/

let globalVar = "I am a global variable"
console.log(globalVar);
	// console.log(localVar); //Error

function showNames() {
		// Function scope variables
	var functionvar = "Joe";
	const functionConst = "John";
	let functionLet = "Jane";

	console.log(functionvar);
	console.log(functionConst);
	console.log(functionLet);
}
showNames();

	// The variables, functionvar, functionconst and functionlet are functions scope variables. They can only be accessed inside of the function they were declared in.
		// console.log(functionvar);
		// console.log(functionConst);
		// console.log(functionLet);

	// Nested function
	// You can create another function inside a function. This is called a nested function. This nested function, being inside the myNewFunction will have access to the variable, "name" as they are within the same code block/scope
function myNewFunction() {
	let name = 'maria'
	function nestedFunction() {
		let nestedName = 'Jose'
		console.log(name);
		console.log(nestedName);
	}
	nestedFunction();

}

myNewFunction ();

// Function and global scope variables

	// Global variables
let globalName = 'Erven Joshua';
function myNewFunction2 () {
	let nameInside = 'Jenno';
	console.log(globalName);
}
myNewFunction2();
// console.log(nameInside); //nameInside is not defined

// Using Alert Method ()
function showSampleAlert() {
alert('Hello World')
}

showSampleAlert()
console.log('I will only log in the console when the alert is dismissed!');

// Using Prompt

/*
	Syntax:
	prompt('<DialogInString>');
*/

let samplePrompt = prompt('Enter your name:')
console.log('Hello, ' + samplePrompt)
console.log(typeof samplePrompt)

/*function printWelcomeMessage() {

	let firstName = prompt ('Enter your first name: ');
	let lastName = prompt ('Enter your last name: ');

	// console.log('Hello, ' + firstName + ' ' + lastName + '!');
	// console.log('Welcome to my page!');

	alert('Hello, ' + firstName + ' ' + lastName + '!');
	alert('Welcome to my page!');
}

printWelcomeMessage()*/

// Function Naming Convention
	// Name your functions in small caps. Follows camelCase notation when naming variables and functions

	function displayCarInfo() {
		console.log('Brand: Toyota')
		console.log('Type: Sedan')
		console.log('Price: 1,500,000')
	}
	displayCarInfo();

	// Functions names should be definitive of the task it will perform. It usually contains a verb

	function getCourse() {
		let course = ['Science 101', 'Math 101', 'English 101']
		console.log(course)
	}
	getCourse()

	// Avoid generic names to avoid confusion within your code

	function get() {
		let name ='jamie'
		console.log(name)
	}
	get(name)

	// Avoid pointless and inapproriate function names










